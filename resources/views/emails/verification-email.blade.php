{{--@component('mail::message')--}}

{{--Click the button below to verify your email address and finish setting up your profile.--}}

{{--@component('mail::button', ['url' => url('/verifyemail/'.$email_token) ])--}}
{{--Verify Email Address--}}
{{--@endcomponent--}}

{{--Thanks,<br>--}}
{{--{{ config('app.name') }}--}}
{{--@endcomponent--}}
<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>

<body>
<h2>Click the button below to verify your email address and finish setting up your profile.</h2>
<br/>
Your registered email-id is {{$user['email']}} , Please click on the below link to verify your email account
<br/>
<a href="{{url('/verifyemail/'.$email_token)}}">Verify Email</a>
</body>

</html>