@extends('layouts.master')

@section('image',asset(''))
@section('page_header')
    <div class="col-lg-8 col-md-10 ">
        <p style="color: white"> Published Lists </p>
    </div>
@endsection

@section('content')
    <div class="col-lg-12 col-md-12 mx-auto" style="height: 100%">
        <div class="panel panel-default">
            <div class="panel-heading"></div>
            <div class="panel-body">
                <table class="table table-hover table-condensed datatable" style="width:100%">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Post title</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('datatable.getdata') }}',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'title', name: 'title'},
                {data: 'body', name: 'body'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });

    </script>
@endsection