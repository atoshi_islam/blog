@extends('layouts.master')

@section('image',asset('/uploads/'.$post->post_image))

@section('page_header')
    <div class="col-lg-8 col-md-10 mx-auto">
        <div class="site-heading">
            <h2> {{$post->title}} </h2>
        </div>
    </div>
@endsection

@section('content')
    <div class="col-lg-12 col-md-12 mx-auto">
        <h2 class="section-heading">{{$post->title}}</h2>
        <span class="subheading">{{$post->sub_title}}</span>
        <p>
            {!! $post->body !!}
        </p>
        {{--<img class="img-fluid" src="{{asset('/uploads/'.$post->post_image)}}" alt="Demo Image">--}}

        @if (Auth::user() && Auth::user()->can('update', $post))
            <div class="form-group">
                <a class="btn btn-primary" href="{{ route('post.edit',$post->id) }}"><i class="fas fa-pen-alt"></i>Edit</a>
            </div>
        @endif

        {{--<hr>--}}
        <div class="comments">
            <ul class="list-group">
                @foreach($post->comments as $comment)
                    <li class="list-group-item">
                        {{$comment->created_at->diffForHumans().', '}} by {{$comment->user->name}} <br>
                        {{$comment->body}}
                    </li>
                @endforeach
            </ul>
        </div>

        <hr>

        {{--add a comment--}}
        @if(Auth::check())
            <div class="card">
                <div class="card-body">
                    <form action="{{route('post.comments',$post->id)}}" method="POST">

                        {{csrf_field()}}
                        <input type="hidden" name="user_id" value="{{auth()->id()}}">

                        <div class="form-group">
                            <textarea name="body" id="" placeholder="Your comment here..."
                                      class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}"></textarea>

                            @if ($errors->has('body'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('body') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Add comment</button>
                        </div>
                    </form>

                </div>
            </div>
        @endif
    </div>


@endsection

