@extends('layouts.master')

@section('image',asset('template/img/home-bg.jpg'))

@section('page_header')
    <div class="col-lg-8 col-md-10 mx-auto">
        <div class="site-heading">
            <h1> See the posts</h1>
            <span class="subheading"></span>
        </div>
    </div>
@endsection

{{--content--}}
@section('content')
    <div class="col-lg-12 col-md-12 mx-auto">
        @if (session('status'))
            <div class="alert alert-danger">
                {{ session('status') }}
            </div>
        @endif

        {{--show the pages--}}
        @foreach($posts as $post)
             <div class="post-preview card-body">
                {{--post title--}}
                <a href="{{route('post.show',$post->id)}}">
                    <h2 class="post-title">
                        {{$post->title}}
                    </h2>
                    <h3 class="post-subtitle">
                        {{$post->sub_title}}
                    </h3>
                </a>
                {{--Limit teaser to 100 characters--}}
                {{--<p class="teaser">--}}
                    {!! str_limit($post->body, 150) !!}
                {{--</p>--}}
                <p class="post-meta">Posted by
                    <a href="{{route('user.profile',$post->user->id)}}">{{$post->user->name}}</a> on {{$post->created_at->toFormattedDateString()}}
                </p>
                {{--count of the comments of posts--}}
                <p class="post-meta">
                    <span class="badge badge-light">
                    {{$post->comments->count()}} <i class="fas fa-comment"></i>
                    </span>
                </p>
            </div>
            <hr>
        @endforeach
        {{-- add laravel default pagination--}}
        {{ $posts->links()}}
    </div>
@endsection

{{--@section('sidebar')--}}
    {{--<div class="container">--}}
        {{--<div class="p-3 mb-3 bg-light rounded">--}}
            {{--<h4 class="font-italic">About</h4>--}}
            {{--<p class="mb-0"></p>--}}
        {{--</div>--}}
        {{--<div class="p-3">--}}
            {{--<h4 class="font-italic">Archives</h4>--}}
            {{--<ol class="list-unstyled mb-0">--}}
                {{--<li><a href="#">March 2014</a></li>--}}
                {{--<li><a href="#">February 2014</a></li>--}}
                {{--<li><a href="#">January 2014</a></li>--}}
                {{--<li><a href="#">December 2013</a></li>--}}
                {{--<li><a href="#">November 2013</a></li>--}}
                {{--<li><a href="#">October 2013</a></li>--}}
            {{--</ol>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--@endsection--}}
