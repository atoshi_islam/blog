@extends('layouts.master')

@section('image',asset(''))
@section('page_header')
    <div class="col-lg-12 col-md-10 ">
        <p style="color: white"> Want to share your story? Fill out the form below to create your post! </p>
    </div>
@endsection

@section('content')
    <div class="col-lg-12 col-md-12 mx-auto">

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        {{--<p>Want to share your story? Fill out the form below to create your post!</p>--}}
        <form method="POST" enctype="multipart/form-data" action="{{route('post.store')}}">
            {{csrf_field()}}

            {{--post title--}}
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" id="title"
                       name="title">

                @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>

            {{--post sub title--}}
            <div class="form-group">
                <label for="sub_title">Subtitle</label>
                <input type="text" class="form-control{{ $errors->has('sub_title') ? ' is-invalid' : '' }}"
                       id="sub_title" name="sub_title">

                @if ($errors->has('sub_title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('sub_title') }}</strong>
                    </span>
                @endif
            </div>

            {{--post body--}}
            <div class="form-group">
                <label for="body">Body</label>
                <textarea placeholder="Post details!" id="body" name="body" rows="10"
                          class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}">
                </textarea>

                @if ($errors->has('body'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('body') }}</strong>
                    </span>
                @endif
            </div>

            {{--post image--}}
            <div class="form-group">
                <label>Post Image</label>
                <input type="file" name="post_image"
                       class="form-control-file{{ $errors->has('post_image') ? ' is-invalid' : '' }}">
                <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size
                    of image should not be more than 2MB.
                </small>

                @if ($errors->has('post_image'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('post_image') }}</strong>
                    </span>
                @endif
            </div>

            {{--image caption--}}
            <div class="form-group">
                <label for="image_caption">Caption</label>
                <input type="text" class="form-control" id="image_caption" name="image_caption">
            </div>
            {{--submit the form--}}
            <div class="form-group">
                <button type="submit" class="btn btn-success"><i class="far fa-plus-square"></i>&nbsp;Publish</button>
                <a class="btn btn-primary" href="{{ route('posts.lists') }}"><i class="fa fa-tasks fa-fw" aria-hidden="true"></i>List</a>
            </div>
            <br>
        </form>
    </div>
@endsection

@section('js')
    <script>
        $('#body').ckeditor();
    </script>
@endsection
