{{--<div class="blog-post">--}}
{{--<h4 class="blog-post-title">--}}
{{--<a href="{{route('post.show',$post->id)}}">--}}
{{--{{$post->title}}--}}
{{--</a>--}}
{{--</h4>--}}
{{--<span class="badge badge-light">{{$post->comments->count()}} {{ str_plural('comment', $post->comments->count()) }}</span>--}}
{{--<p class="blog-post-meta">{{$post->created_at->toFormattedDateString()}} by <a href="#">{{$post->user->name}}</a></p>--}}
{{--<hr>--}}
{{--<a href="{{ route('post.show', $post->id ) }}">--}}
{{--<p class="teaser">--}}
{{--{{  str_limit($post->body, 100) }} --}}{{-- Limit teaser to 100 characters --}}
{{--</p>--}}
{{--</a>--}}
{{----}}

{{--</div><!-- /.blog-post -->--}}

<span class="badge badge-light">{{$post->comments->count()}} {{ str_plural('comment', $post->comments->count()) }}</span>
<p >{{$post->created_at->toFormattedDateString()}} by <a href="#">{{$post->user->name}}</a></p>
<h2>{{$post->title}}</h2>
<p>{{$post->body}}</p>
<p>
    <a class="btn btn-secondary" href="{{route('post.show',$post->id)}}" role="button">View details &raquo;</a>
</p>