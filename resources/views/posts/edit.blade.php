@extends('layouts.master')

@section('image',asset(''))
@section('page_header')
    <div class="col-lg-8 col-md-10 ">
        <p style="color: white"> Want to edit your shared story? Edit the form below ! </p>
    </div>
@endsection

@section('content')
    <div class="col-lg-12 col-md-12 mx-auto">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <form method="POST" enctype="multipart/form-data" action="{{route('post.update',$post->id)}}">
            {{csrf_field()}}

            {{--post title--}}
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" id="title"
                       name="title" value="{{$post->title}}">

                @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>

            {{--post sub title--}}
            <div class="form-group">
                <label for="sub_title">Subtitle</label>
                <input type="text" class="form-control{{ $errors->has('sub_title') ? ' is-invalid' : '' }}"
                       id="sub_title" name="sub_title" value="{{$post->sub_title}}">

                @if ($errors->has('sub_title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('sub_title') }}</strong>
                    </span>
                @endif
            </div>


            {{--post body--}}
            <div class="form-group">
                <label>Body</label>
                <textarea placeholder="Post details!" id="body" name="body" rows="10"
                          class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}">
                    {{$post->body}}
                </textarea>

                @if ($errors->has('body'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('body') }}</strong>
                    </span>
                @endif
            </div>

            {{--post image--}}
            <div class="form-group">
                <label>Post Image</label>
                <input type="file" class="form-control-file{{ $errors->has('post_image') ? ' is-invalid' : '' }}"
                       name="post_image" value="{{$post->post_image}}">
                <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size
                    of image should not be more than 2MB.
                </small>

                @if ($errors->has('post_image'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('post_image') }}</strong>
                    </span>
                @endif
            </div>

            {{--image caption--}}
            <div class="form-group">
                <label for="image_caption">Caption</label>
                <input type="text" class="form-control" id="image_caption" name="image_caption"
                       value="{{$post->image_caption}}">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success"><i class="fas fa-pen-alt"></i>&nbsp;Update </button>
                <a class="btn btn-primary" href="{{ route('create') }}"><i class="far fa-plus-square"></i>&nbsp;Publish</a>
                <a class="btn btn-primary" href="{{ route('post.show',$post->id) }}"><i class="fas fa-eye"></i> Show</a>
            </div>
        </form>
        <form method="POST" action="{{route('post.delete',$post->id)}}">
            {{csrf_field()}}
            <input type="submit" class="btn btn-danger" value="Delete"/>
        </form>
    </div>
@endsection

@section('js')
    <script>
        $('#body').ckeditor();
    </script>
@endsection