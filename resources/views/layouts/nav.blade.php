<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <h1><a class="navbar-brand" href="{{ url('/') }}">
                <i class="fa fa-home fa-fw" aria-hidden="true"></i> {{ config('app.name') }}
            </a></h1>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto"></ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    <li class="nav-item">
                        @if (Route::has('register'))
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        @endif
                    </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('user.profile',Auth::user()->id)}}" role="button">
                                <i class="fas fa-user-alt"></i>&nbsp;{{Auth::user()->name}}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{route('create')}}" role="button">
                                <i class="far fa-plus-square"></i>&nbsp;{{ __('Publish Post') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('posts.lists') }}" role="button">
                                <i class="fa fa-tasks fa-fw" aria-hidden="true"></i>{{ __('Lists') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
document.getElementById('logout-form').submit();">
                                <i class="fas fa-sign-out-alt"></i>{{ __('Logout') }}
                            </a>
                        </li>
                        {{--<li class="nav-item dropdown">--}}
                        {{--<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"--}}
                        {{--data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>--}}
                        {{--<span class="caret"></span>--}}
                        {{--</a>--}}
                        {{--<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">--}}
                        {{--<a class="dropdown-item" href="{{ route('logout') }}"--}}
                        {{--onclick="event.preventDefault();--}}
                        {{--document.getElementById('logout-form').submit();">--}}
                        {{--{{ __('Logout') }}--}}
                        {{--</a>--}}
                        {{--</div>--}}
                        {{--</li>--}}
                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            @csrf
                        </form>
                        @endguest
            </ul>
        </div>
    </div>
</nav>

