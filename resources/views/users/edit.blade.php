@extends('layouts.master')

@section('image',asset(''))
@section('page_header')
    <div class="col-lg-12 col-md-10 ">
        <p style="color: white"> Want to edit your profile ? Fill out the form below to edit your profile! </p>
    </div>
@endsection

@section('content')

        <div class="col-lg-10 order-lg-2 mx-auto">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form method="POST" enctype="multipart/form-data" action="{{ route('user.update',$user->id)}}">
                {{csrf_field()}}
                {{--name--}}
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                           id="name" name="name" value="{{ $user->name }}">
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>

                {{--email--}}
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           id="email" name="email" value="{{ $user->email }}">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>

                {{--password--}}
                <div class="form-group">
                    <label for="password">Password</label>
                    <input id="password" type="password"
                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password">

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>

                {{--password confirmation--}}
                <div class="form-group">
                    <label for="password-confirm">Confirm Password</label>
                    <input id="password-confirm" type="password" class="form-control"
                           name="password_confirmation">
                </div>

                {{--User bio--}}
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <textarea id="bio" class="form-control" rows="10" name="user_bio">{{$user->user_bio}}</textarea>
                </div>

                <div class="form-group">
                    <label>Profile</label>
                    <input type="file" name="avatar"
                           class="form-control-file{{ $errors->has('avatar') ? ' is-invalid' : '' }}">
                    <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size
                        of image should not be more than 2MB.
                    </small>

                    @if ($errors->has('avatar'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('avatar') }}</strong>
                                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success"><i class="fas fa-pen-alt"></i>&nbsp;Update</button>
                    <a class="btn btn-primary" href="{{ route('user.profile',$user->id) }}"><i
                                class="fas fa-user-alt"></i>&nbsp;Profile</a>
                </div>
            </form>
        </div>
        @if($user->avatar)
            <div class="col-lg-2 order-lg-1 text-center">
                <img src="{{asset('/uploads/'.Auth::user()->avatar)}}" class="mx-auto img-fluid img-circle d-block"
                     alt="avatar">
                <span class="caption text-muted">{{$user->name}}'s Picture</span>
            </div>
        @endif


@endsection

@section('js')
    <script>
        $('#bio').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
    </script>
@endsection