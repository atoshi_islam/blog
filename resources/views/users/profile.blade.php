@extends('layouts.master')

@section('image',asset(''))
@section('page_header')
    <div class="col-lg-12 col-md-10 ">
        <p style="color: white"> {{$user->name}}'s bio!! </p>
    </div>
@endsection

@section('content')
    <div class="col-lg-8 order-lg-2">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a href="" data-target="#profile" data-toggle="tab" class="nav-link active"><i
                            class="fas fa-user-alt"></i>&nbsp;Profile</a>
            </li>

            @if(Auth::user()->can('update', $user))
                <li class="nav-item">
                    <a href="{{route('user.edit',$user->id)}}" class="nav-link"><i class="fas fa-user-edit"></i>Edit</a>
                </li>
            @endif

        </ul>
        <div class="tab-content py-4">
            <div class="tab-pane active" id="profile">
                <h5><i class="mb-3">{{$user->name}}'s Profile</i></h5>
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="mt-2"><span class="fa fa-clock-o ion-clock float-right"></span> About me!</h5>
                        <table class="table table-sm table-hover table-striped">
                            <tbody>
                            <tr>
                                <td>
                                    {!! $user->user_bio !!}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--/row-->
            </div>
        </div>
    </div>
    {{--if user has profile image --}}
    <div class="col-lg-4 order-lg-1 text-center">
        @if($user->avatar)
            <img src="{{asset('/uploads/'.$user->avatar)}}" class="mx-auto img-fluid img-circle d-block"
                 alt="avatar">
        @endif
    </div>


@endsection