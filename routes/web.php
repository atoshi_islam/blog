<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'PostsController@index');
//for seeing the index page
Route::get('/home', 'PostsController@index')->name('home');
//for creating the post
Route::get('/posts/create', ['as' => 'create', 'uses' => 'PostsController@create']);
//for storing the post
Route::post('/posts/post', ['as' => 'post.store', 'uses' => 'PostsController@store']);
//for retriveing the posts.For this page no authentication is required
Route::get('/posts/{post}/show', ['as' => 'post.show', 'uses' => 'PostsController@show']);
//for edit the individual post
Route::get('/posts/{post}/edit', ['as' => 'post.edit', 'uses' => 'PostsController@edit']);
//for update the individual post
Route::post('/posts/{post}/update', ['as' => 'post.update', 'uses' => 'PostsController@update']);

Route::post('/posts/{post}/delete', ['as' => 'post.delete', 'uses' => 'PostsController@destroy']);


//for commenting of a post
Route::post('/posts/{post}/comments', ['as' => 'post.comments', 'uses' => 'CommentsController@store']);

Route::get('/posts/lists', ['as' => 'posts.lists', 'uses' => 'DataTableController@datatable']);
Route::get('datatable/getdata', 'DataTableController@getPosts')->name('datatable.getdata');

//routes for users
Route::get('/user/{user}/edit', ['as' => 'user.edit', 'uses' => 'UsersController@edit']);
Route::post('/user/{user}/update', ['as' => 'user.update', 'uses' => 'UsersController@update']);
Route::get('/user/{user}/profile', ['as' => 'user.profile', 'uses' => 'UsersController@profile']);

//Email Verification
Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');
