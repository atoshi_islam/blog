<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class UsersController extends Controller
{
    //
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile(User $user) {
        return view('users.profile', ['user' => $user]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit(Request $request, User $user) {
        if ($request->user()->can('update', $user)) {
            return view('users.edit', ['user' => Auth::user()]);
        }
        return back()->with('status', 'You cant access this user!');
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, User $user) {
        //
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['string', 'email', 'max:255'],
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        // get current logged in user
        if ($request->user()->can('update', $user)) {
            $name = $request->name;
            $email = $request->email;
            $bio = $request->user_bio;
            //$user_bio = $request->user_bio;
            $user->update(['name' => $name, 'email' => $email, 'user_bio' => $bio]);

            if ($request->hasFile('avatar')) {
                $avatar = $request->file('avatar');
                $filename = time() . '.' . $avatar->getClientOriginalExtension();
                Image::make($avatar)->save(public_path('\uploads/' . $filename));
                $user = Auth::user();
                $user->avatar = $filename;
                $user->save();
            }
            return redirect()->route('user.edit', $user->id)->with('status', 'User has been updated!');
        }
        else{
            return back()->with('status', 'You cant access this user!');
        }
    }
}
