<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Post;
use Illuminate\Support\Facades\Auth;
use PhpParser\Comment;

class CommentsController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    //
    public function store(Post $post) {

        $this->validate(request(), ['body' => 'required|min:2']);

        // return redirect()->route('posts.show', $post->id);
        $post->addComment(request(['body', 'user_id']));

        return back();
    }
}
