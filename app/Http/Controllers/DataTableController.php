<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class DataTableController extends Controller
{
    //

    /**
     * Load the view of post data table
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function datatable() {
        return view('posts.lists');
    }

    /**
     * Get the posts belongs to the users
     *
     * @return mixed
     */
    public function getPosts() {
        $user = Auth::user();

        $datatble = DataTables::of($user->posts);

        return $datatble
            ->editColumn('id', function ($posts) {
                return '<a href=" ' . route('post.edit', $posts->id) . ' ">' . $posts->id . '</a>';
            })
            ->editColumn('title', function ($posts) {
                return str_limit($posts->title,40 );
            })
            ->editColumn('body', function ($posts) {
                return '<a href=" ' . route('post.show', $posts->id) . ' ">' . str_limit($posts->body, 80) . '</a>';
            })
            ->addColumn('action', function ($posts) {
                return '<a href=" ' . route('post.edit', $posts->id) . ' " class="btn btn-primary"><i class="fas fa-pen-alt"></i> Edit</a>';
            })->escapeColumns([])->make(true);
        //return DataTables::of(Post::query())->make(true);

    }
}
