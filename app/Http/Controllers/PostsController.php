<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBlogPost;
use App\Post;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class PostsController extends Controller
{
    /**
     * Authentication not need in index and details pages of posts
     * PostsController constructor.
     */
    public function __construct() {
        $this->middleware(['auth'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //get the latest 3 posts
        //dd('test');
        $posts = Post::latest()->paginate(3);
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreBlogPost $request
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreBlogPost $request, Post $post) {
        // Retrieve the validated input data...
        if ($request->validated()) {
            //$post = new Post;
            $post->title = $request->title;
            $post->sub_title = $request->sub_title;
            $post->body = $request->body;
            $post->image_caption = $request->image_caption;
            $post->user_id = auth()->id();
            //check if request has file type
            if ($request->hasFile('post_image')) {
                $post_image = $request->file('post_image');
                $filename = time() . '.' . $post_image->getClientOriginalExtension();
                Image::make($post_image)->save(public_path('\uploads/' . $filename));
                $post->post_image = $filename;
            }
            $post->save();
            return redirect()->route('post.edit', $post->id)->with('status', 'Your post has been created!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Post $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Post $post) {
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Post $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Post $post) {
        // get current logged in user
        $user = Auth::user();
        //check if looged in user has the ability to edit the post
        if ($user->can('update', $post)) {
            return view('posts.edit', compact('post'));
        } else {
            echo 'Not Allowed to edit the post';
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreBlogPost $request
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreBlogPost $request, Post $post) {
        // get current logged in user
        $user = Auth::user();
        //check if looged in user has the ability to edit the post
        if ($user->can('update', $post)) {
            // Retrieve the validated input data...
            if ($request->validated()) {
                $title = $request->title;
                $sub_title = $request->sub_title;
                $body = $request->body;
                $image_caption = $request->image_caption;

                //update the post
                $post->update(['title' => $title, 'sub_title' => $sub_title, 'body' => $body, 'image_caption' => $image_caption]);

                //check if request has file type
                if ($request->hasFile('post_image')) {
                    $post_image = $request->file('post_image');
                    $filename = time() . '.' . $post_image->getClientOriginalExtension();
                    Image::make($post_image)->save(public_path('\uploads/' . $filename));
                    $post->post_image = $filename;
                    $post->save();
                }
                return redirect()->route('post.edit', $post->id)->with('status', 'Your post has been updated!');
            }
        } else {
            echo 'Not Allowed';
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post) {
        // get current logged in user
        $user = Auth::user();
        //if the post is belongs to that user then delete
        if ($user->can('delete', $post)) {
            $post->delete();
            return redirect()->route('home')->with('status', 'Your post has been deleted!');
        } else {
            echo 'Not Allowed';
        }
    }
}
