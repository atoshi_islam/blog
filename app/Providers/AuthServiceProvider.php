<?php

namespace App\Providers;

use App\Policies\PostPolicy;
use App\Policies\UserPolicy;
use App\Post;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Post::class => PostPolicy::class,
        User::class => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     * In the boot method, we've defined our custom gate:
     * @return void
     */
    public function boot() {
        $this->registerPolicies();
        //Gate::resource('posts', 'App\Policies\PostPolicy');
        //Gate::resource('users', 'App\Policies\UserPolicy');
         //Gate::define('update-post', 'PostPolicy@update');
         //Gate::define('delete-post', 'PostPolicy@delete');
    }
}
